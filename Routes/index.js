var path = require("path");

module.exports = function (router, mysql, pool,io) {
	router.get('/', function (req, res) {
		res.sendFile(path.join(__dirname, '../Client', '/login.html'));
	});
	router.get('/sw.js', function (req, res) {
		res.sendFile(path.join(__dirname, '../Client', '/sw.js'));
	});
	router.get('/service-worker.js', function (req, res) {
		res.sendFile(path.join(__dirname, '../Client', '/service-worker.js'));
	});

	router.get('/status', function (req, res) {
		res.sendFile(path.join(__dirname, '../Client', '/index.html'));
	});
	router.get('/icon', function (req, res) {
		res.sendFile(path.join(__dirname, '../Client', '/icon.png'));
	});

	router.get('/getStatus', function (req, res) {
		pool.getConnection(function (err, connection) {
			if (err) {
				connection.release();
				return res.json({ "error": true, "message": "Error in database." });
			} else {
				var sqlQuery = "SELECT * FROM ??";
				var inserts = ["UserPost"];
				sqlQuery = mysql.format(sqlQuery, inserts);
				connection.query(sqlQuery, function (err, rows) {
					connection.release();
					if (err) {
						return res.json({ "error": true, "message": "Error in database." });
					} else {
						res.json({ "error": false, "message": rows });
					}
				});
			}
			connection.on('error', function (err) {
				return res.json({ "error": true, "message": "Error in database." });
			});
		});
	});
	router.post("/login", (req, res) => {
		pool.getConnection(function (err, connection) {
			if (err) {
				connection.release();
				return res.json({ "error": true, "message": "Error in database." });
			} else {
				var sqlQuery = "SELECT * FROM user WHERE UserName = '" + req.body.username + "'";
				connection.query(sqlQuery, function (err, rows) {
					connection.release();
					if (err) {
						return res.json({ "error": true, "message": "Error in database." });
					} else {
						if (rows[0] && rows[0].Password === req.body.password)
							res.json({ "error": false, "message": rows });
						else res.json({ "error": true, "message": "Login fail" });
					}
				});
			}
			connection.on('error', function (err) {
				return res.json({ "error": true, "message": "Error in database." });
			});
		});
	});
	router.get('/broadcast', function (req, res) {
		res.sendFile(path.join(__dirname, '../Client', '/broadcastnotify.html'));
	});
	router.post("/broadcast", (req, res) => {
		io.emit("notify everyone", { data: req.body.data });
	});
};
