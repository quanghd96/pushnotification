var express = require('express');
var app = express();
var path = require("path");
var mysql = require("mysql");
var http = require('http').Server(app);
var io = require('socket.io')(http);
var router = express.Router();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var pool = mysql.createPool({
	connectionLimit: 100,
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'socketDemo',
	debug: false
});


var { addComment, seen, getListNotSeen } = require("./db");
var routes = require("../Routes/")(router, mysql, pool, io);

app.use('/', router);

http.listen(3000, function () {
	console.log("Listening on 3000");
});

io.on('connection', function (socket) {
	console.log('We have user connected !');
	socket.on('getListNotSeen', data => {
		getListNotSeen(data.token, pool, (err, result) => {
			io.emit('notify not seen', {token: data.token, count: result.length})
		})
	})
	socket.on('comment added', function (data) {
		addComment(data.user, data.comment, mysql, pool, function (error, result) {
			if (error) {
				io.emit('error');
			} else {
				io.emit("notify someone", { user: data.user, comment: data.comment, listToken: result, idNotification: 0 });
			}
		});
	});
	socket.on('seen', function (data) {
		seen(data.token, data.idNotification, mysql, pool, function (error, result) {
			if (error) {
				io.emit('error');
			} else {
				console.log(data.token + " seen " + data.idNotification);
			}
		});
	});

});
