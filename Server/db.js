function addComment(user, comment, mysql, pool, callback) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "INSERT into ?? (??,??,??) VALUES ((SELECT ?? FROM ?? WHERE ?? = ?),?,?)";
			var inserts = ["UserComment", "UserId", "UserName", "Comment", "UserId", "User", "UserName", user, user, comment];
			sqlQuery = mysql.format(sqlQuery, inserts);
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					var sqlQuery = "SELECT Token FROM userposttoken WHERE PostId = '0'";
					connection.query(sqlQuery, function (err, rows) {
						connection.release();
						if (err) {
							return callback(true, null);
						} else {
							callback(false, rows);
						}
					});
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};

function seen(token, idNotification, mysql, pool, callback) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "UPDATE userposttoken SET seen = 1 WHERE PostId = " + idNotification + " AND Token = '" + token + "'";
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					connection.release();
					return callback(false, null);
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};
function getListNotSeen(token, pool, callback) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "SELECT * FROM `userposttoken` WHERE seen = 0 AND Token = '" + token + "'";
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					connection.release();
					return callback(false, rows);
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};

module.exports = {
	addComment,
	seen,
	getListNotSeen
}